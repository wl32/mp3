module.exports = function errorHandler(err, req, res, next) {
    console.error(err)
    // internal server error
    res.status(500).send('something has failed')
}