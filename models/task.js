const mongoose = require('mongoose')
const Joi = require('joi')

const taskSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        // name is unique
        unique: true,
    },
    description: {
        type: String,
        default: '',
    },
    completed: {
        type: Boolean,
        default: false,
    },
    assignedUser: {
        type: String,
        ref: 'User',
        default: ''
    },
    assignedUserName: {
        type: String,
        default: 'unassigned'
    },
    deadline: {
        type: Date,
        required: true,
    },
    dateCreated: {
        type: Date,
        default: Date.now,
    },
})


const Task = mongoose.model('Task', taskSchema)

const validate = function(task) {
    const schema = Joi.object({
        name: Joi.string().required(),
        deadline: Joi.date().required(),
        description: Joi.string(),
        assignedUser: Joi.string(),
        assignedUserName: Joi.string(),
        completed: Joi.boolean(),

    })
    return schema.validate(task)
}

module.exports = {
    Task,
    validate,
}