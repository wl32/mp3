const mongoose = require('mongoose')
const Joi = require('joi')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    pendingTasks: {
        type: [String],
        default: function() {
            return []
        },
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    dateCreated: {
        type: Date,
        default: Date.now,
    },
})


const User = mongoose.model('User', userSchema)

const validate = function(user) {
    const schema = Joi.object({
        name: Joi.string().required(),
        email: Joi.string().required().email(),
        pendingTasks: Joi.array(),
    })
    return schema.validate(user)
}

module.exports = {
    User,
    validate,
}
