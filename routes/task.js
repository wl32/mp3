const express = require('express')
const router = express.Router()
const { Task, validate } = require('../models/task')
const { User, validate1 } = require('../models/user')
// Respond with a List of tasks
router.get('/', async (req,res)=>{
    let { where,sort,select,skip,limit,count} = req.query
    where = where?JSON.parse(where):{}
    sort = sort?JSON.parse(sort):{}
    select = select?JSON.parse(select):{}
    // skip should be great than or equal to 0
    skip = Math.max(skip?Number(skip):0, 0)
    limit = limit?Number(limit):Number.MAX_SAFE_INTEGER
    let task = Task.find(where).skip(skip).limit(limit).sort(sort).select(select)
    if(count&&count=='true') {
        task = task.count()
    }
    const result = await task
    res.send({
        message: 'ok',
        data: result
    })
})
// Respond with details of specified task or 404 error
router.get('/:id', async (req,res)=>{
    const task = await Task.findById(req.params.id)   // .populate('assignedUser')  // optional
    if(!task) return res.status(404).send('The task with the given ID is not exist')
    res.send({
        message: 'ok',
        data: task
    })
})
// Create a new task. Respond with details of new task
router.post('/', async (req,res)=>{
    const { error } = validate(req.body)
    if(error) return res.status(400).send(error.message)

    let task = await Task.findOne({name: req.body.name})
    if(task) return res.status(400).send('The task is already exist.')
    //验证user 不存在则 ""
    let user =  User.findOne({_id:req.body.assignedUser,name:req.body.assignedUserName})
    user = await user
    console.log(user)
    if(!user) {req.body.assignedUser='unassigned';req.body.assignedUserName='unassigned';}

    task = new Task(req.body)
    task.save()
    res.status(201).send({
        message: 'Create a new task success',
        data: task
    })
})
// Replace entire task with supplied task or 404 error
router.put('/:id', async (req,res)=>{
    const task = await Task.findById(req.params.id)
    if(!task) return res.status(404).send('The task with the given ID is not exist')

    task.set(req.body)
    task.save()
    res.send({
        message: 'Replace entire task seccess',
        data: task
    })
})
// Delete specified task or 404 error
router.delete('/:id', async (req,res)=>{
    const task = await Task.findByIdAndRemove(req.params.id)
    if(!task) return res.status(404).send('The task with the given ID is not exist')
    // 同时删除用户任务
    await User.updateMany({$pull:{"pendingTasks":req.params.id}})
    res.send({
        message: 'Delete specified task success',
        data: task
    })
})

module.exports = router;