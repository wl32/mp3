const { User, validate } = require('../models/user')
const _ = require('lodash')
const express = require('express')
const router = express.Router()
const { Task, validate1 } = require('../models/task')
// Create a new user. Respond with details of new user
router.post('/', async (req,res)=>{
    const { error } = validate(req.body)
    if(error) return res.status(400).send(error.message)

    let user = await User.findOne({email:req.body.email})
    //不存在则404
    if(user) return res.status(400).send('This email has already been registered.')
    
    //验证task 不存在则 []
    new_task_list = []
    for (var i = 0; i < req.body.pendingTasks.length; i ++) { 
        let task =  Task.findOne({_id:req.body.pendingTasks[i]})
        task = await task
        if(!task) {new_task_list.push(req.body.pendingTasks[i])}
    }
    console.log(new_task_list)
    user = new User({
        name: req.body.name,
        email: req.body.email,
        pendingTasks: new_task_list||[],
    })
    user.save()
    res.status(201).send({
        message: 'Create a new user success',
        data: user
    })
})
// Respond with a List of users
router.get('/', async (req,res)=>{
    let { where,sort,select,skip,limit,count} = req.query
    where = where?JSON.parse(where):{}
    sort = sort?JSON.parse(sort):{}
    select = select?JSON.parse(select):{}
    // skip should be great than or equal to 0
    skip = Math.max(skip?Number(skip):0, 0)
    limit = limit?Number(limit):Number.MAX_SAFE_INTEGER
    let task = User.find(where).skip(skip).limit(limit).sort(sort).select(select)
    task = await task
    // console.log(task)
    if(task.length===0) return res.status(404).send('The user with the given ID is not exist')
    if(count&&count=='true') {
        task = task.length
    }
    const result = task
    res.send({
        message: 'Respond with a List of users seccess',
        data: result
    })
})
// Respond with details of specified user or 404 error
router.get('/:id', async (req,res)=>{
    const user = await User.findById(req.params.id)
    console.log(111,user)
    if(!user) return res.status(404).send('The user with the given ID is not exist')
    res.send({
        message: 'user detail seccess',
        data: user
    })
})
// Replace entire user with supplied user or 404 error
router.put('/:id', async (req,res)=>{
    const user = await User.findById(req.params.id)
    if(!user) return res.status(404).send('The user with the given ID is not exist')
    const update = {}
    if(req.body.name) {
        update.name = req.body.name
    }
    if(req.body.email) {
        update.email = req.body.email
    }
    if(req.body.pendingTasks) {
        update.pendingTasks = req.body.pendingTasks
    }
    user.set(update)
    user.save()
    res.send(user)
})
// Delete specified user or 404 error
router.delete('/:id', async (req,res)=>{
    const user = await User.findByIdAndRemove(req.params.id)
    //删除属于用户任务
    await Task.find({assignedUser:req.params.id}).update({assignedUser:""})
    if(!user) return res.status(404).send('The user with the given ID is not exist')
    res.send({
        message: 'delete seccess',
        data: user
    })
})
module.exports = router;