// Get the packages we need
const express = require('express')

// Create our Express application
const app = express()

require('./setup/exception');
require('./setup/config')();
// load DB and routes module
require('./setup/db')();
require('./setup/routes')(app);

// Use environment defined port or 4000
const port = process.env.PORT||4000;
// Start the server
app.listen(port, ()=>console.log(`Server is running on port ${port}`))