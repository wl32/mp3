const config = require('config')

// 首先检测是否设置了数据库连接配置项，否则程序终止.
module.exports = function () {
    if(!config.get('mongo_connection_url')) {
        throw new Error('Fatal Error: mongo_connection_url was not set.')
    }
}