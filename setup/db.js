const mongoose = require('mongoose')
// `config` module is a node module for server-side configuration
const config = require('config')

module.exports = function () {
    // get config from `config/default.json`
    mongoose.connect(config.get('mongo_connection_url'))
    .then(()=>{
        console.info('Connected to database...')
    })
}