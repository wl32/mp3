process.on('unhandledRejection', err=>{
    throw err;
})
process.on('uncaughtException', err=>{
    console.error('Something seems to be wrong: ',err)
    // exit process
    process.exit(1)
})