const express = require('express')
const helmet = require('helmet')
const userRoute = require('../routes/user')
const taskRoute = require('../routes/task')
const cors = require('../middlewares/cors')
const errorHandler = require('../middlewares/errorHandler')

// setup middlewares and register routes for app
module.exports = function (app) {
    app.use(cors())
    app.use(express.static('public'))
    app.use(express.json())
    app.use(express.urlencoded({extended:false}))
    app.use(helmet())

    app.use('/api/users', userRoute)
    app.use('/api/tasks', taskRoute)
    
    app.use(errorHandler)
}